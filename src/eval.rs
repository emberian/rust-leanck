//! The evaluator and type checker.

use ast::*;
use vec_map::VecMap;

pub struct Environment {
    /// Map from constant index to its body and type.
    constants: VecMap<(Rc<Term>, Rc<Term>)>,
    universes: VecMap<Universe>,
}

pub struct Context {
    variables: LinkedList<Rc<Term>>,
}

