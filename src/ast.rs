pub enum ParameterKind {
    Class,
    Infer,
    Binder,
    Universe,
}

fn rc_val<T>(x: &Rc<T>) -> usize {
    let y: &T = &*x;
    y as usize
}

pub struct NameIdx(u32);

pub struct UniverseIdx(u32);

pub enum Universe {
    Zero,
    Succ(UniverseIdx),
    Max(UniverseIdx, UniverseIdx),
    IMax(UniverseIdx, UniverseIdx),
    Param(NameIdx),
}

/// A term of the language.
pub enum Term {
    /// De-Bruijn index
    Var(u32),
    /// A sort denoting a particular universe
    Sort(UniverseIdx),
    /// A constant instantiated with particular universe parameters
    Constant(NameIdx, Vec<UniverseIdx>),
    /// Application of a function
    App(Rc<Term>, Rc<Term>),
    /// Lambda abstraction
    Abs {
        typ: Rc<Term>,
        body: Rc<Term>,
    },
    /// Pi abstraction
    Pi {
        typ: Rc<Term>,
        body: Rc<Term>,
    },
    /// The value of an axiom is "magical" -- we assume it exists, but don't know what it is. Only its type.
    Magical(NameIdx),
}

/// Definition of a name.
pub struct Def {
    typ: Rc<Term>,
    body: Rc<Term>,
    universe_params: Vec<NameIdx>,
}

/// Definition of an inductive type
pub struct Ind {
    typ: Rc<Term>,
    intros: Vec<(NameIdx, Rc<Term>)>,
    universe_params: Vec<NameIdx>,
}