extern crate cons_list;

use std::rc::Rc;
use cons_list::ConsList;

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
enum Name {
    Global(String), // fixme: interning
    Local(u32),
    Quote(u32),
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
enum TermInfer {
    Annotated(Rc<TermCheck>, Rc<TermCheck>),
    Bound(u32),
    Free(Name),
    Star,
    Pi(Rc<TermCheck>, Rc<TermCheck>),
    App(Rc<TermInfer>, Rc<TermCheck>),
}

#[derive(PartialEq, PartialOrd, Hash, Clone, Debug)]
struct Thunk {
    to_apply: Rc<TermCheck>,
    env: Env,
}

impl Thunk {
    fn apply(&self, arg: Rc<Value>) -> Rc<Value> {
        eval_check(self.to_apply.clone(), self.env.append(arg))
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
enum TermCheck {
    Infer(Rc<TermInfer>),
    Lam(Rc<TermCheck>),
}

#[derive(PartialEq, PartialOrd, Hash, Clone, Debug)]
enum Value {
    Lam(Thunk),
    Star,
    Pi(Rc<Value>, Thunk),
    Neutral(Rc<Neutral>),
}

#[derive(PartialEq, PartialOrd, Hash, Clone, Debug)]
enum Neutral {
    Free(Name),
    App(Rc<Neutral>, Rc<Value>),
}

fn vfree(x: Name) -> Rc<Value> {
    // todo: cache
    Rc::new(Value::Neutral(Rc::new(Neutral::Free(x))))
}

fn vapp(a: Rc<Value>, b: Rc<Value>) -> Rc<Value> {
    match &*a {
        &Value::Lam(ref th) => th.apply(b),
        &Value::Neutral(ref n) => Rc::new(Value::Neutral(Rc::new(Neutral::App(n.clone(), b)))),
        _ => panic!("interpreter tried to apply non-function!")
    }
}

#[derive(PartialEq, PartialOrd, Hash, Clone, Debug)]
struct Env {
    // lol double rc
    values: ConsList<Rc<Value>>,
}

impl Env {
    fn append(&self, v: Rc<Value>) -> Env {
        Env { values: self.values.append(v) }
    }

    fn empty() -> Env {
        Env { values: ConsList::new() }
    }
}

fn eval_infer(exp: Rc<TermInfer>, d: Env) -> Rc<Value> {
    use TermInfer::*;
    match &*exp {
        &Annotated(ref exp, ref ty) => eval_check(exp.clone(), d),
        &Free(ref x) => vfree(x.clone()),
        &Bound(idx) => d.values.iter().nth(idx as usize).expect("type checking should ensure the env is well-formed").clone(),
        // todo: cache
        &Star => Rc::new(Value::Star),
        &Pi(ref tc1, ref tc2) => Rc::new(Value::Pi(eval_check(tc1.clone(), d), Thunk { to_apply: tc2.clone(), env: d})),
        &App(ref ti, ref tc) => vapp(eval_infer(ti.clone(), d.clone()), eval_check(tc.clone(), d))
    }
}

fn eval_check(exp: Rc<TermCheck>, d: Env) -> Rc<Value> {
    use TermCheck::*;
    match &*exp {
        &Infer(ref ti) => eval_infer(ti.clone(), d),
        &Lam(ref tc) => Rc::new(Value::Lam(Thunk { to_apply: tc.clone(), env: d })),
    }
}


#[derive(Clone, Debug)]
struct Context {
    types: ConsList<(Name, Rc<Value>)>,
}

impl Context {
    fn lookup(&self, n: &Name) -> Option<&Rc<Value>> {
        for &(ref k, ref v) in self.types.iter() {
            if k == n {
                return Some(v);
            }
        }
        None
    }

    fn append(&self, n: Name, i: Rc<Value>) -> Context {
        Context {
            types: self.types.append((n, i))
        }
    }
}

type Result<T> = std::result::Result<T, String>;


fn type_infer(i: u32, c: Context, e: Rc<TermInfer>) -> Result<Rc<Value>> {
    use TermInfer::*;
    match &*e {
        &Annotated(ref tc, ref t) => {
            type_check(i, c, t.clone(), Rc::new(Value::Star))?; // todo: cache
            let t = eval_check(t.clone(), Env::empty());
            type_check(i, c, tc.clone(), t.clone())?;
            Ok(t)
        },
        &Star => Ok(Rc::new(Value::Star)), // todo: cache
        &Pi(tc1, tc2) => {
            type_check(i, c, tc1.clone(), Rc::new(Value::Star))?;
            let t = eval_check(tc1.clone(), Env::empty());
            type_check(i + 1, 
                       c.append(Name::Local(i), t.clone()), 
                       (subst_check(0, Rc::new(TermInfer::Free(Name::Local(i))), tc2.clone())),
                       Rc::new(Value::Star))?; // todo: cache
            Ok(Rc::new(Value::Star)) // todo: cache
        }
        &Free(ref n) => match c.lookup(n) {
            Some(t) => Ok(t.clone()),
            _ => Err("unknown identifier".into()),
        },
        &App(ref ti, ref tc) => {
            let sigma = type_infer(i, c.clone(), ti.clone())?;
            match &*sigma {
                &Value::Pi(v1, th) => {
                    type_check(i, c, tc.clone(), v1.clone())?;
                    Ok(th.apply(eval_check(tc.clone(), Env::empty())))
                },
                _ => Err("invalid application".into())
            }
        },
        &Bound(_) => Err("can't figure out type of bound variable".into()),
    }
}

fn type_infer_0(c: Context, t: Rc<TermInfer>) -> Result<Rc<Value>> {
    type_infer(0, c, t)
}

fn type_check(i: u32, c: Context, e: Rc<TermCheck>, t: Rc<Value>) -> Result<()> {
    use TermCheck::*;
    match &*e {
        &Infer(ref ti) => {
            let t2 = type_infer(i, c, ti.clone())?;
            if t != t2 {
                Err("type mismatch".into())
            } else {
                Ok(())
            }
        },
        &Lam(ref tc) => {
            match &*t {
                &Value::Pi(ref param, ref th) => {
                    type_check(i + 1, 
                               c.append(Name::Local(i), t.clone()), 
                               subst_check(0, Rc::new(TermInfer::Free(Name::Local(i))), tc.clone()),
                               th.apply(vfree(Name::Local(i))))
                },
                _ => Err("type mismatch".into()),
            }
        }
    }
}

fn subst_infer(i: u32, replace: Rc<TermInfer>, exp: Rc<TermInfer>) -> Rc<TermInfer> {
    use TermInfer::*;
    match &*exp {
        &Annotated(ref tc, ref t) => Rc::new(Annotated(subst_check(i, replace, tc.clone()), subst_check(i, replace, t.clone()))),
        &Bound(j) => if i == j { replace } else { exp.clone() },
        &Star => exp.clone(),
        &Pi(ref tc1, ref tc2) => Rc::new(Pi(subst_check(i, replace, tc1.clone()), subst_check(i + 1, replace, tc2.clone()))),
        &Free(_) => exp.clone(),
        &App(ref e1, ref e2) => Rc::new(App(subst_infer(i, replace.clone(), e1.clone()), subst_check(i, replace, e2.clone()))),
    }
}

fn subst_check(i: u32, replace: Rc<TermInfer>, exp: Rc<TermCheck>) -> Rc<TermCheck> {
    use TermCheck::*;
    match &*exp {
        &Infer(ref e) => Rc::new(Infer(subst_infer(i, replace, e.clone()))),
        &Lam(ref e) => Rc::new(Lam(subst_check(i + 1, replace, e.clone()))),
    }
}