impl TermCache {
    /// Remove terms from the TermCache that are no longer referred to by live values
    fn compact(&mut self) {
        // this is hilariously bad and could use some factoring.
        let &TermCache {
            ref mut to_drop,
            ref mut vars,
            ref mut sorts,
            ref mut apps,
            ref mut abs,
            ref mut pis,
            ..
        } = self;

        to_drop.clear();

        for (k, v) in vars {
            match v.upgrade() {
                Some(_) => { },
                None => to_drop.push(*k as usize),
            }
        }

        for val in to_drop {
            vars.remove(val as u32);
        }

        to_drop.clear();

        for (k, v) in sorts {
            match v.upgrade() {
                Some(_) => { },
                None => to_drop.push(*k as usize),
            }
        }

        for val in to_drop {
            sorts.remove(val as u32);
        }

        to_drop.clear();

        for (k, v) in inner_terms {
            match v.upgrade() {
                Some(_) => { },
                None => to_drop.push(*k),
            }
        }

        for val in to_drop {
            inner_terms.remove(val);
        }

        to_drop.clear();

        for (k, v) in apps {
            match v.upgrade() {
                Some(_) => { },
                None => to_drop.push(*k),
            }
        }

        for val in to_drop {
            apps.remove(val);
        }

        to_drop.clear();

        for (k, v) in abs {
            match v.upgrade() {
                Some(_) => { },
                None => to_drop.push(*k),
            }
        }

        for val in to_drop {
            abs.remove(val);
        }

        to_drop.clear();

        for (k, v) in pis {
            match v.upgrade() {
                Some(_) => { },
                None => to_drop.push(*k),
            }
        }

        for val in to_drop {
            apps.remove(val);
        }

        // keep the drop list smaller than 8MiB
        if to_drop.len() > 1_000_000 {
            to_drop.resize(1_000_000, 0);
        }
    }

    fn var(&mut self, level: u32) -> Rc<Term> {
        match self.vars.entry(level) {
            Entry::Vacant(v) => {
                let val = Rc::new(Term::Var(level));
                v.insert(val.downgrade());
                val
            },
            Entry::Occupied(o) => match o.get().upgrade() {
                Some(v) => v,
                None => {
                    let val = Rc::new(Term::Var(level));
                    o.insert(val.downgrade());
                    val
                }
            }
        }
    }

    fn u(&mut self) -> UniverseCache {
        UniverseCache { self }
    }

    fn const(&mut self, c: u32, univs: Rc<Vec<UniverseIdx>>) -> Rc<Term> {
        Rc::new(Term::Constant(c, univs)) // todo: cache with a prefix trie?
    }

    fn app(&mut self, left: Rc<Term>, right: Rc<Term>) -> Rc<Term> {
        match self.vars.entry(level) {
            Entry::Vacant(v) => {
                let val = Rc::new(Term::App(left, right));
                v.insert(val.downgrade());
                val
            },
            Entry::Occupied(o) => match o.get().upgrade() {
                Some(v) => v,
                None => {
                    let val = Rc::new(Term::App(left, right));
                    o.insert(val.downgrade());
                    val
                }
            }
        }
    }

    fn abs(&mut self, typ: Rc<Term>, body: Rc<Term>) -> Rc<Term> {
        match self.vars.entry(level) {
            Entry::Vacant(v) => {
                let val = Rc::new(Term::App(left, right));
                v.insert(val.downgrade());
                val
            },
            Entry::Occupied(o) => match o.get().upgrade() {
                Some(v) => v,
                None => {
                    let val = Rc::new(Term::App(left, right));
                    o.insert(val.downgrade());
                    val
                }
            }
        }
    }
}