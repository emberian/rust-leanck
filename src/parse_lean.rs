use ast::*;

use vec_map::VecMap;
use std::collections::btree_map::Entry;

pub struct TermIdx(u32);

pub struct ParseContext {
    name_components: VecMap<(String, usize)>,
    universes: VecMap<Universe>,
    constants: VecMap<Def>,
    terms: VecMap<Rc<Term>>,
}

pub enum Errors {
    SyntaxError,
    OrderingError,
}

impl ParseContext {
    pub fn parse_line(&mut self, line: &str) -> Result<(), Error> {
        let mut components = line.split(' ');
        let (num, cmd) = match (components.next(), components.next()) {
            (Some(n), Some(c)) => (n, c.trim()),
            _ => return Err(Error::SyntaxError),
        };
        match cmd {
            "#NS" | "#NI" => {
                let num = num.parse::<usize>()?;
                let parent = match components.next() {
                    Some(p) => p.parse::<u32>()?,
                    None => return Err(Error::SyntaxError),
                };
                let comp = match components.next() {
                    Some(c) => c.trim().to_string(),
                    None => return Err(Error::SyntaxError),
                };
                self.names.set(num, comp);
                Ok(())
            },
            "#US" => {
                let num = num.parse::<usize>()?;
                let parent = match components.next() {
                    Some(p) => p.trim().parse::<u32>()?,
                    None => return Err(Error::SyntaxError),
                };
                self.universes.set(num, Universe::Succ(UniverseIdx(parent)));
            },
            "#UM" => {
                let num = num.parse::<usize>()?;
                let (p1, p2) = match (components.next(), components.next()) {
                    (Some(p1), Some(p2)) => (p1.parse::<u32>()?, p2.trim().parse::<u32>()?),
                    _ => return Err(Error::SyntaxError),
                };
                self.universes.set(num, Universe::Max(UniverseIdx(p1), UniverseIdx(p2)));
            },
            "#UIM" => {
                let num = num.parse::<usize>()?;
                let (p1, p2) = match (components.next(), components.next()) {
                    (Some(p1), Some(p2)) => (p1.parse::<u32>()?, p2.trim().parse::<u32>()?),
                    _ => return Err(Error::SyntaxError),
                };
                self.universes.set(num, Universe::Max(UniverseIdx(p1), UniverseIdx(p2)));
            },
            "#UP" => {
                let num = num.parse::<usize>()?;
                let parent = match components.next() {
                    Some(p) => p.parse::<u32>()?,
                    None => return Err(Error::SyntaxError),
                };
                self.universes.set(NameIdx(num), Universe::Param(parent));
            },
            "#EV" => {
                let num = num.parse::<usize>()?;
                let index = match components.next() {
                    Some(p) => p.trim().parse::<u32>()?,
                    None => return Err(Error::SyntaxError),
                };
                self.terms.set(num, Term::Var(index));
            },
            "#ES" => {
                let num = num.parse::<usize>()?;
                let index = match components.next() {
                    Some(p) => p.trim().parse::<u32>()?,
                    None => return Err(Error::SyntaxError),
                };
                self.terms.set(num, Term::Sort(UniverseIdx(index)));
            },
            "#EC" => {
                let num = num.parse::<usize>()?;
                let mut comps = Vec::new();
                let name = match components.next() {
                    Some(n) => n.trim().parse::<u32>()?,
                    None => return Err(Error::SyntaxError),
                };
                while let Some(c) = components.next() {
                    comps.push(UniverseIdx(c.trim().parse::<u32>()));
                }
                self.terms.set(num, Rc::new(Term::Constant(NameIdx(name), comps)));
            },
            "#EA" => {
                let num = num.parse::<usize>()?;
                let (p1, p2) = match (components.next(), components.next()) {
                    (Some(p1), Some(p2)) => (p1.parse::<usize>()?, p2.trim().parse::<usize>()?),
                    _ => return Err(Error::SyntaxError),
                };
                let nt = match (self.terms.get(p1).map(|x| x.clone()), self.terms.get(p2).map(|x| x.clone())) {
                    (Some(p1), Some(p2)) => Rc::new(Term::App(p1, p2)),
                    _ => return Err(Error::OrderingError)
                };
                self.terms.set(num, nt);
            },
            "#EL" | "#EP" => {
                let num = num.parse::<usize>()?;
                let _ = components.next(); // todo: binder info
                let name = match components.next() {
                    Some(n) => n.parse::<u32>()?,
                    None => return Err(Error::SyntaxError),
                };
                let (p1, p2) = match (components.next(), components.next()) {
                    (Some(p1), Some(p2)) => (p1.parse::<usize>()?, p2.trim().parse::<usize>()?),
                    _ => return Err(Error::SyntaxError),
                };
                let nt = match (self.terms.get(p1).map(|x| x.clone()), self.terms.get(p2).map(|x| x.clone())) {
                    (Some(p1), Some(p2)) => if cmd == "#EL" { 
                        Rc::new(Term::Abs { typ: p1, body: p2 }) 
                    } else { 
                        Rc::new(Term::Pi { typ: p1, body: p2 })
                    },
                    _ => return Err(Error::OrderingError)
                };
                self.terms.set(num, nt);
            },
            _ => {
                match num {
                    "#DEF" | "#AX" => {
                        let name = match components.next() {
                            Some(n) => n.parse::<u32>()?,
                            None => return Err(Error::SyntaxError),
                        };
                        let typ = match components.next() {
                            Some(n) => {
                                let idx = n.parse::<usize>()?;
                                match self.terms.get(idx) {
                                    Some(t) => t.clone(),
                                    None => return Err(Error::OrderingError),
                                }
                            },
                            None => return Err(Error::SyntaxError),
                        };
                        let body = if num != "#AX" {
                            match components.next() {
                                Some(n) => {
                                    let idx = n.parse::<usize>()?;
                                    match self.terms.get(idx) {
                                        Some(t) => t.clone(),
                                        None => return Err(Error::OrderingError),
                                    }
                                },
                                None => return Err(Error::SyntaxError),
                            }
                        } else {
                            Rc::new(Term::Magical(NameIdx(name)))
                        };
                        let comps = Vec::new();
                        while let Some(n) = components.next() {
                            comps.push(NameIdx(n.trim().parse::<u32>()?));
                        }
                    },
                    "#IND" => {

                    },
                    "#PREFIX" => {

                    },
                    _ => Err(Error::SyntaxError)
                }
            }
        }
    }
}